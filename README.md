# WebSocket Pear Relay: A WebSocket to Holepunch message relayer

## Why?
This is built to enable developing web apps using Holepunch technology, since Holepunch Hyperswaem doesn't support browsers (or rather they don't support it).

## How to build
1. `npx parcel build`
This is to bundle the files together and get them ready for the next point
2. `npx pkg ./dist/relay.js`
This will create executables for windows, linux, and mac.

