import DHT from '@hyperswarm/dht'
import goodbye from 'graceful-goodbye'
import b4a from 'b4a'
import cenc from 'compact-encoding'
import { WebSocketServer } from 'ws';
export class Engine {
    constructor() {
        this.dht = new DHT()
        this.keyPair = DHT.keyPair()
        this.state = cenc.state()
        this.wss = new WebSocketServer({ port: 8080 });
    }

    run() {
        console.log("server started")
        const server = this.dht.createServer(conn => {
            this.conn = conn
            this.ws.send(JSON.stringify(["UPDATE", "PEAR_JOINED"]))
            conn.on('data', data => {
                let message = cenc.decode(cenc.raw.string, data)
                message = JSON.parse(message)
                this.ws.send(JSON.stringify(message))
            })
        })
        server.listen(this.keyPair).then(() => {
            this.ws.send(JSON.stringify(["WAITING_ON", b4a.toString(this.keyPair.publicKey, 'hex')]))
        })
        goodbye(() => server.close())
    }

    connect(publicKey = this.serverPublicKey) {
        console.log("connecting to server")
        this.ws.send(JSON.stringify(["CONNECTING", JSON.stringify(b4a.toString(publicKey, 'hex'))]))
        
        this.conn = this.dht.connect(publicKey)
        this.conn.on("error", error => {
            console.log(error.message.split(":")[0])
            this.ws.send(JSON.stringify(["ERROR", error.message]))
        })
        this.conn.on('data', data => {
            let message = cenc.decode(cenc.raw.string, data)
            message = JSON.parse(message)
            this.ws.send(JSON.stringify(message))
        })

        this.conn.once('open', () => {
            this.ws.send(JSON.stringify(["UPDATE", "JOINED"]))
        })

    }
    
    serve(serverMode, publicKey) {
        if (serverMode) {
            this.run()
        } else {
            this.serverPublicKey = b4a.from(publicKey, "hex")
            this.connect()
        }
    }

    initializeWebSocket(engine = this) {
        this.wss.on('connection', function connection(ws) {
            console.log("GUI Connected");
            engine.ws = ws;
            ws.on('error', console.error);
            ws.on('message', function message(data) {
                data = JSON.parse(data);
                if (JSON.parse(data)[0]=="NEW_GAME") engine.serve(true)
                if (JSON.parse(data)[0]=="JOIN_GAME") engine.serve(false, JSON.parse(data)[1])
                if (engine.conn)
                    engine.conn.write(data)
            });
        });
    }
}


